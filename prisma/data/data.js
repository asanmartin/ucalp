export const professors = [
	{"firstName": "CRISTIAN PABLO", "lastName": "AGUILAR"},
	{"firstName": "MARíA GABRIELA", "lastName": "ALCOLUMBRE"},
	{"firstName": "IGNACIO ANDRES", "lastName": "BONANATA"},
	{"firstName": "CECILIA BEATRIZ", "lastName": "BORRONI"},
	{"firstName": "CLARISA RITA", "lastName": "DE ARCE"},
	{"firstName": "JUAN MANUEL", "lastName": "FACCIOLO"},
	{"firstName": "AMALIA FANNY", "lastName": "FENSORE"},
	{"firstName": "BEATRIZ DEL CARMEN", "lastName": "FORCLAZ"},
	{"firstName": "MARIA VERONICA", "lastName": "LA ROCA"}
]

export const careers = [
	{"nameCareer": "Lic. en Criminalística"},
	{"nameCareer": "Escribanía"},
	{"nameCareer": "Lic. en Fonoaudiología"},
	{"nameCareer": "Lic. en Kinesiología y Fisiatría"},
	{"nameCareer": "Lic. en Nutrición"},
	{"nameCareer": "Lic. en Terapia Ocupacional"},
	{"nameCareer": "Ciclo de Profesorado Universitario"}
]

export const subjects = [
	{"nameSubject": "Papiloscopía y Rastros I", "duration": "Anual", "year": 1},
	{"nameSubject": "Química Aplicada I", "duration": "Anual", "year": 1},
	{"nameSubject": "Balística Forense I", "duration": "Anual", "year": 1},
	{"nameSubject": "Identidad Humana", "duration": "1° Cuatrimestre", "year": 1},
	{"nameSubject": "Derecho Penal", "duration": "1° Cuatrimestre", "year": 1},
	{"nameSubject": "xxxxxxxxx", "duration": "xxxxxxxxxxxxxx", "year": 2},
	{"nameSubject": "Papiloscopía y Rastros II", "duration": "Anual", "year": 2},
	{"nameSubject": "Química Aplicada II", "duration": "Anual", "year": 2},
	{"nameSubject": "Balística Forense II", "duration": "Anual", "year": 2},
	{"nameSubject": "Accidentología Vial I", "duration": "Anual", "year": 2},
	{"nameSubject": "Técnicas de la Escena del Crimen II", "duration": "Anual", "year": 2},
	{"nameSubject": "Documentología I", "duration": "Anual", "year": 3},
	{"nameSubject": "Accidentología Vial II", "duration": "Anual", "year": 3},
	{"nameSubject": "Tecnología Aplicada", "duration": "Anual", "year": 3},
	{"nameSubject": "Estadística", "duration": "1° Cuatrimestre", "year": 3},
	{"nameSubject": "Informática Aplicada", "duration": "1° Cuatrimestre", "year": 3},
	{"nameSubject": "Toxicología y Química Legal", "duration": "Anual", "year": 4},
	{"nameSubject": "Documentología II", "duration": "Anual", "year": 4},
	{"nameSubject": "Metodología de la Investigación Científica", "duration": "1° Cuatrimestre", "year": 4},
	{"nameSubject": "Medicina Legal", "duration": "1° Cuatrimestre", "year": 4},
	{"nameSubject": "Inglés", "duration": "1° Cuatrimestre", "year": 4}
]

export const classrooms = [
	{"classroomNumber": 110, "description": "Solo tiene sillas", "capacity": 20},
	{"classroomNumber": 120, "description": "Solo tiene mesas", "capacity": 15},
	{"classroomNumber": 130, "description": "Solo tiene camillas", "capacity": 17},
	{"classroomNumber": 220, "description": "Aula completa sin calefacción", "capacity": 40},
	{"classroomNumber": 250, "description": "Aula de piso radiante", "capacity": 40},
	{"classroomNumber": 270, "description": "Sin ventanas", "capacity": 40}
]

export const subjectInProfessor = [
	{"subjectId": 1, "professorId": 1},
	{"subjectId": 2, "professorId": 1},
	{"subjectId": 3, "professorId": 2},
	{"subjectId": 4, "professorId": 3},
	{"subjectId": 5, "professorId": 4},
	{"subjectId": 6, "professorId": 5},
	{"subjectId": 7, "professorId": 5}
]

export const subjectInCareer = [
	{"subjectId": 1, "careerId": 1},
	{"subjectId": 2, "careerId": 2},
	{"subjectId": 3, "careerId": 1},
	{"subjectId": 4, "careerId": 2},
	{"subjectId": 5, "careerId": 4},
	{"subjectId": 6, "careerId": 3},
	{"subjectId": 7, "careerId": 4}
]